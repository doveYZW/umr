static struct umr_bitfield mmCLK1_0_CLK1_CLK_PLL_REQ[] = {
	 { "FbMult_int", 0, 8, &umr_bitfield_default },
	 { "FbMult_frac", 16, 31, &umr_bitfield_default },
};
static struct umr_bitfield mmCLK1_0_CLK1_CLK0_BYPASS_CNTL[] = {
	 { "CLK0_BYPASS_SEL", 0, 2, &umr_bitfield_default },
	 { "CLK0_BYPASS_DIV", 16, 19, &umr_bitfield_default },
};
static struct umr_bitfield mmCLK1_0_CLK1_CLK1_BYPASS_CNTL[] = {
	 { "CLK1_BYPASS_SEL", 0, 2, &umr_bitfield_default },
	 { "CLK1_BYPASS_DIV", 16, 19, &umr_bitfield_default },
};
static struct umr_bitfield mmCLK1_0_CLK1_CLK2_BYPASS_CNTL[] = {
	 { "CLK2_BYPASS_SEL", 0, 2, &umr_bitfield_default },
	 { "CLK2_BYPASS_DIV", 16, 19, &umr_bitfield_default },
};
static struct umr_bitfield mmCLK1_0_CLK1_CLK3_DS_CNTL[] = {
	 { "CLK3_DS_DIV_ID", 0, 2, &umr_bitfield_default },
};
static struct umr_bitfield mmCLK1_0_CLK1_CLK3_ALLOW_DS[] = {
	 { "CLK3_ALLOW_DS", 0, 0, &umr_bitfield_default },
};
static struct umr_bitfield mmCLK1_0_CLK1_CLK3_BYPASS_CNTL[] = {
	 { "CLK3_BYPASS_SEL", 0, 2, &umr_bitfield_default },
	 { "CLK3_BYPASS_DIV", 16, 19, &umr_bitfield_default },
};
static struct umr_bitfield mmCLK1_0_CLK1_CLK0_CURRENT_CNT[] = {
	 { "CURRENT_COUNT", 0, 31, &umr_bitfield_default },
};
static struct umr_bitfield mmCLK1_0_CLK1_CLK1_CURRENT_CNT[] = {
	 { "CURRENT_COUNT", 0, 31, &umr_bitfield_default },
};
static struct umr_bitfield mmCLK1_0_CLK1_CLK2_CURRENT_CNT[] = {
	 { "CURRENT_COUNT", 0, 31, &umr_bitfield_default },
};
static struct umr_bitfield mmCLK1_0_CLK1_CLK3_CURRENT_CNT[] = {
	 { "CURRENT_COUNT", 0, 31, &umr_bitfield_default },
};
